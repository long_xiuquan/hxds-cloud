package com.example.hxds.bff.driver.service;

import com.example.hxds.bff.driver.controller.form.InsertOrderGpsForm;

import java.util.ArrayList;

public interface OrderGpsService {

    public int insertOrderGps(InsertOrderGpsForm form);
}
